# from distutils.core import setup
from setuptools import find_packages, setup

setup(
      name='Ronhill Tools',
      version='0.2.5',
      description='My Tools',
      author='Ronhill',
      author_email='ronhill.ilya@gmail.com',
      packages=find_packages(),
      install_requires=[line.strip() for line in open("requirements.txt")],
)