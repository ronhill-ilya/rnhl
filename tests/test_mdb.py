from unittest import TestCase
from rnhl.ext import mdb
from flask import Flask


class TestFlaskMDBExtention(TestCase):
    def test_crud_model(self):
        class TestModel(mdb.Model):
            name = mdb.StringProperty(required=True)

        app = Flask('test')
        app.config['MDB_HOST'] = "192.168.99.102"
        app.config['MDB_PORT'] = 27017
        app.config['MDB_DBNAME'] = "nosetest_app"
        mdb.FlaskMDBExtention(app)

        @app.route("/remove_all")
        def remove_all():
            for o in TestModel.query().fetch():
                o.key.delete()
            return "ok"

        @app.route("/create")
        def create_all():
            o = TestModel(name="test")
            o.put()
            return str(TestModel.query().count())

        @app.route("/count")
        def count():
            return str(TestModel.query().count())

        with app.test_client() as client:
            assert client.get('/remove_all').data == "ok"
            assert client.get('/count').data == "0"
            assert client.get('/create').data == "1"
            assert client.get('/create').data == "2"
            assert client.get('/create').data == "3"








