from PIL import Image
from StringIO import StringIO


def get_size(image_data):
    img_src = Image.open(image_data)
    return img_src.size


def crop_image(image_data, left=0, top=0, right=0, bottom=0):
    img_src = Image.open(image_data)
    width, height = img_src.size
    _left = left
    _top = top
    _right = width - right
    _bottom = height - bottom
    cropped_img = img_src.crop((_left, _top, _right, _bottom))
    tmp_io = StringIO()
    cropped_img.save(tmp_io, "jpeg")
    tmp_io.seek(0)
    return tmp_io