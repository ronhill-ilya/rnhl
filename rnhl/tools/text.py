# -*- coding: utf-8 -*-
import goslate

_symbols = (u"абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ",
           u"abvgdeejzijklmnoprstufhzcss_y_euaABVGDEEJZIJKLMNOPRSTUFHZCSS_Y_EUA")

_tr = dict( [ (ord(a), ord(b)) for (a, b) in zip(*_symbols) ] )

_gs = goslate.Goslate()

def transliterate(text):
    return text.translate(_tr)


def translate(text, out_lang="ru"):
    return _gs.translate(text, out_lang)


