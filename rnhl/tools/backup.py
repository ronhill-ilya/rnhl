import sh
from bakthat.helper import BakHelper

def mongo(database, tags=None):
    import logging
    logging.basicConfig(level=logging.INFO, format="%(asctime)s;%(name)s;%(levelname)s;%(message)s")
    MONGODUMP_PARAMS = {'d': database}
    # glacier or s3
    DESTINATION = "glacier"
    tags = tags or []

    # BakHelper automatically create a temp directory
    with BakHelper("mymongobak", destination="glacier", password='', tags=tags) as bh:
        logging.info(sh.mongodump(**MONGODUMP_PARAMS))
        bh.backup()
        bh.rotate()


if __name__ == "__main__":
    mongo('acquario_1', tags=['test'])