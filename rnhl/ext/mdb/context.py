import model

from flask import g

from pymongo.collection import Collection
from gridfs import GridFS


class Context(object):
    def __init__(self, pymongo):
        self._mongo_db = pymongo.db
        self._deserialise_entity = {}

    def get_or_insert(self, key, for_update=False, **kwargs):
        cls = model.Model.get_cls_by_kind(key._get_kind())
        obj = self.get(key)
        if obj is None:
            obj = cls(
                key=key,
                **kwargs
            )
            obj.put()
        return obj

    def _get_mongo_collection(self, key):
        """
        :param rnhl.ext.mdb.key.Key key: key
        :rtype: Collection
        """
        return Collection(self._mongo_db, key._get_kind())

    def get_gridfs(self):
        """
        :rtype GridFS
        """
        return GridFS(self._mongo_db)

    def get_mongo_collection(self, kind):
        """
        :rtype: Collection
        """
        return Collection(self._mongo_db, kind)


    def delete(self, key):
        """
        :param rnhl.ext.mdb.key.Key key: key
        """
        _mongo = self._get_mongo_collection(key)
        _mongo.remove({"_id": key.mongo_key})


    def build_entity(self, cls, key, data):
        entity = cls(key=key)
        if data is None:
            return None
        for property in cls._properties.values():
            value = data.get(property._name, None)
            if value is not None and value != [None]:
                property._deserialize(entity, value)

        return entity

    def get(self, key):
        """

        :param rnhl.ext.mdb.key.Key key: key
        :return:
        """

        _mongo = self._get_mongo_collection(key)
        cls = model.Model.get_cls_by_kind(key._get_kind())

        data = _mongo.find_one({'_id': key.mongo_key})
        entity = self.build_entity(cls, key, data)
        if entity:
            entity._post_get()
            return entity

    def get_multi(self, keys):
        return [self.get(k) for k in keys]

    def put(self, entity):
        """

        :param amt.ext.rdb.model.Model entity: entity
        :rtype : bool
        """

        _mongo = self._get_mongo_collection(entity.key)
        cls = entity.__class__
        data = {'_id': entity.key.mongo_key}
        uninitialized_p = []
        for property in cls._properties.values():
            property._prepare_for_put(entity)
            if not property._is_initialized(entity):
                uninitialized_p.append(property._name)
            data[property._name] = property._serialize(entity)
        if uninitialized_p:
            raise ValueError("%r has uninitialised properties: %r" % (entity._get_kind(), uninitialized_p))
        if data:
            _mongo.save(data)

    def put_multi_async(self, entities):
        for e in entities:
            self.put(e)


def get_context(**ctx_options):
    """
    :param namespace:
    :rtype: Context
    """
    if not g:
        raise RuntimeError("work outsite request context")
    return g.mdb_context

