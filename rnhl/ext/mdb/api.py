from flask import request, make_response
from bson import json_util
import json
from flask.ext.restful import Api, Resource
from .key import Key


class MdbModelItemsResource(Resource):
    def __init__(self, model_cls):
        """
        :param mdb.Model model_cls: model_cls
        """
        self._model_cls = model_cls

    def _build_filters(self, filters_raw):
        _filters = []
        for filter_name, value in json.loads(filters_raw).items():
            if filter_name.endswith('.key_url_safe'):
                filter_name = filter_name.replace('.key_url_safe', '')
                value = Key(urlsafe=value)
            elif filter_name.endswith('.$date'):
                filter_name = filter_name.replace('.$date', '')
                value = json_util.object_hook({'$date': value})

            _filters.append(getattr(self._model_cls, filter_name) == value)

        return _filters

    def _build_order(self, sort_field, sort_dir):
        sort_field = sort_field if sort_field != 'id' else "key"
        if sort_dir == 'ASC':
            return +getattr(self._model_cls, sort_field)
        else:
            return -getattr(self._model_cls, sort_field)

    def get(self):
        args = request.args
        filters = self._build_filters(request.args.get('_filters', json.dumps({})))
        sort_field = request.args.get('_sortField', "key")
        sort_dir = request.args.get('_sortDir', 'ASC')
        order = self._build_order(sort_field, sort_dir)
        limit = int(request.args.get('_perPage', 30))
        page = int(request.args.get('_page', 1))
        skip = (page - 1) * limit
        _res = []
        query = self._model_cls.query(*filters).order(order)
        for i in query.fetch(limit=limit, skip=skip):
            _res.append(i.to_dict())
        return _res, 200, {
            'Access-Control-Expose-Headers': 'x-total-count',
            'X-Total-Count': query.count(),
            }

    def post(self, action=None):
        if action is not None:
            print self._model_cls
            meth = getattr(self._model_cls, action, None)
            if not meth:
                return "Action not found %r" % action, 404
            if not hasattr(meth, 'is_api_method'):
                return "Action not allowed %r" % action, 405
            try:
                return meth(**request.json)
            except TypeError as e:
                return e.message, 400
        else:
            obj = self._model_cls()
            obj.populate(request.json)
            obj.put()
            return obj.to_dict()



class MdbModelItemResource(Resource):
    def __init__(self, model_cls):
        """
        :param mdb.Model model_cls: model_cls
        """
        self._model_name = model_cls

    def _get_by_urlsafe(self, key_url_safe):
        """
        :rtype: mdb.Model
        """
        key = Key(urlsafe=key_url_safe)
        return key.get()


    def get(self, key_url_safe):
        obj = self._get_by_urlsafe(key_url_safe)
        if not obj:
            return {"error": "Not found entity"}, 404
        return obj.to_dict()

    def post(self, key_url_safe, action=None):
        if action is None:
            return {"error", "action argument required"}, 400
        obj = self._get_by_urlsafe(key_url_safe)
        if not obj:
            return {"error": "Not found entity"}, 404
        if action is None:
            return "Action required", 400
        meth = getattr(obj, action, None)
        if not meth:
            return "Action not found %r" % action, 404

        if not hasattr(meth, 'is_api_method'):
            return "Action not alowed %r" % action, 405

        try:
            return meth(**request.json)
        except TypeError as e:
            return e.message, 400


    def delete(self, key_url_safe):
        obj = self._get_by_urlsafe(key_url_safe)
        if not obj:
            return {"error": "Not found entity"}, 404

        obj.key.delete()
        return {
            "id": obj.key.urlsafe()
        }

    def put(self, key_url_safe):
        obj = self._get_by_urlsafe(key_url_safe)
        if not obj:
            return {"error": "Not found entity"}, 404
        obj.populate(request.json)
        obj.put()
        return obj.to_dict()


class MdbApi(Api):
    def register_models(self, *models, **kwargs):
        for model in models:
            self.add_resource(MdbModelItemsResource,
                             '/mdb/%s' % model._get_kind(),
                             '/mdb/%s_actions/<string:action>' % model._get_kind(),
                             resource_class_kwargs=dict(model_cls=model),
                             endpoint="mdb.%s.items" % model._get_kind().lower(),
                             subdomain=kwargs.get('subdomain')
                             )
            self.add_resource(MdbModelItemResource,
                             '/mdb/%s/<string:key_url_safe>' % model._get_kind(),
                             '/mdb/%s/<string:key_url_safe>/<string:action>' % model._get_kind(),
                             resource_class_kwargs=dict(model_cls=model),
                             endpoint="mdb.%s.item" % model._get_kind().lower(),
                             subdomain=kwargs.get('subdomain')
            )



api = MdbApi(prefix="/_admin")


@api.representation('application/json')
def output_json(data, code, headers=None):
    resp = make_response(json.dumps(data, default=json_util.default), code)
    resp.headers.extend(headers or {})
    return resp

