from flask import g
from flask_pymongo import PyMongo
from .context import Context


py_mongo = PyMongo()


class FlaskMDBExtention(object):
    def __init__(self, app=None):
        self.app = None
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        """

        :param flask.Flask app: flask app
        """
        py_mongo.init_app(app, config_prefix="MDB")

        @app.before_request
        def before_request():
            g.mdb_context = Context(py_mongo)