import pickle
import base64
import json
from bson import json_util
import model

from context import get_context


class Key(object):
    URLSAFE_SEPARATE = ":"

    @classmethod
    def from_mongo_key(cls, mongo_key):
        if not mongo_key:
            return None
        kind, id = mongo_key['kind'], mongo_key['id']
        return Key(kind, id, parent=cls.from_mongo_key(mongo_key['parent']))

    def __init__(self, kind=None, id=None, parent=None, urlsafe=None):
        if isinstance(kind, type) and issubclass(kind, model.Model):
            kind = kind._get_kind()
        elif kind and model.Model._kind_map.get(kind) is None:
            raise ValueError('kind %r mast bee in _kind_map' % kind )

        if parent and not isinstance(parent, Key):
            raise ValueError('parent mast bee instance of Key')

        if kind is not None and id is not None:
            self._parent = parent
            self._kind = kind
            self._id = id
        elif urlsafe is not None:
            self._parent, self._kind, self._id = self._from_urlsafe(urlsafe)
        else:
            raise ValueError("Only one of ((kind, id), urlsafe) mast bee passed")

    def _get_kind(self):
        return self._kind

    def _get_index_name(self):
        return "rdb_" + self._kind.lower()

    def has_parent(self):
        return self._parent is not None

    def get(self, for_update=False, skip_locked=False, **ctx_options):
        return get_context(**ctx_options).get(self)

    def delete(self, **ctx_options):
        return get_context(**ctx_options).delete(self)

    def __repr__(self):
        if self._parent is not None:
            return "Key(%r, %r, parent=%r)" % (self._get_kind(), self._id, self._parent)
        return "Key(%r, %r)" % (self._get_kind(), self._id)

    def _urlsafe_decode(self, urlsafe):
        if not isinstance(urlsafe, basestring):
            raise TypeError('urlsafe must be a string; received %r' % urlsafe)
        if isinstance(urlsafe, unicode):
            urlsafe = urlsafe.encode('utf8')
        mod = len(urlsafe) % 4
        if mod:
            urlsafe += '=' * (4 - mod)
        # This is 3-4x faster than urlsafe_b64decode()
        return json.loads(
            base64.b64decode(urlsafe.replace('-', '+').replace('_', '/')),
            object_hook=json_util.object_hook
        )

    def _urlsafe_encode(self, data):
        urlsafe = base64.b64encode(json.dumps(data, default=json_util.default))
        return urlsafe.rstrip('=').replace('+', '-').replace('/', '_')

    def _from_urlsafe(self, urlsafe):
        _parent = None
        parts = urlsafe.split(self.URLSAFE_SEPARATE)
        if len(parts) > 1:
            _parent = Key(urlsafe=parts[0])
            _kind, _id = self._urlsafe_decode(parts[1])
        else:
            _kind, _id = self._urlsafe_decode(parts[0])
        return _parent, _kind, _id

    def urlsafe(self):
        encoded = self._urlsafe_encode((self._kind, self._id))
        if self._parent is not None:
            return self.URLSAFE_SEPARATE.join([self._parent.urlsafe(), encoded])
        return encoded

    @property
    def keys_list(self):
        res = []
        if self._parent is not None:
            res += self._parent.keys_list
        res.append(self.__class__(self._kind, self._id))
        return res

    @property
    def mongo_key(self):
        return {
            "id": self._id,
            "kind": self._kind,
            "parent": None if not self._parent else self._parent.mongo_key
        }

    def id(self):
        return self._id

    def kind(self):
        return self._kind

    def parent(self):
        """
        :rtype: None | Key
        """
        return self._parent

    def __eq__(self, other):
        if other.__class__ is not self.__class__:
            return False
        return self._id == other._id and self._kind == other._kind

    def __ne__(self, other):
        return not self.__eq__(other)


def _validate_key(value, entity=None):
    """

    :param rnhl.ext.mdb.key.Key value: value
    :param rnhl.ext.mdb.model.Model entity: entity
    :return:
    """
    if not isinstance(value, Key):
        raise ValueError('Expected Key, got %r' % value)
    if entity and value._get_kind() != entity._get_kind():

        raise ValueError("Can't set key o entity with another kind %s != %s" % (value._get_kind(), entity._get_kind()))
    return value

