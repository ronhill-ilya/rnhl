import json
import logging
import time

from .context import get_context
from .key import Key


class Cursor(object):
    def __init__(self, id):
        self._id = id

    def to_redis_scan(self):
        return int(self._id)


def NOT(*args):
    data = Query._prepare_query_args(args)
    if len(data) != 1:
        raise ValueError("Too many properties")
    name, op, value = data[0]
    if op != Operations.RANGE:
        raise ValueError("'NOT' do not support operation %r only %s" % (op, Operations.RANGE))
    return name, Operations.RANGE_NOT, value


class Operations(object):
    MATCH = "match"
    FUZZY = "fuzzy"

    EQ = "eq"
    IN = "in"
    NE = "ne"

    ISNULL = "isnull"

    MISSING = "missing"
    NOT_MISSING = "not_missing"

    RANGE = "range"
    RANGE_NOT = "range_not"
    GTE = "$gte"
    GT = "$gt"
    LTE = "$lte"
    LT = "$lt"
    _RANGE_OPS = [GTE, GT, LTE, LT]
    _RANGE_OPS_GROUPS = {GT: '$gt', GTE: '$gt', LTE: '$lt', LT: '$lt'}


class QueryIterator(object):
    cursor = None
    cached = None
    more = False
    kwargs = {}

    def __init__(self, query, **kwargs):
        self.query = query
        self.kwargs = kwargs or {}
        if not 'limit' in kwargs:
            self.kwargs['limit'] = 10

    def __iter__(self):
        return self

    def fetch_page(self):
        return self.query.fetch_page(cursor=self.cursor, **self.kwargs)

    def next(self):
        if not self.cached:
            self.cached, self.more, self.cursor = self.fetch_page()
        if not self.cached:
            raise StopIteration
        return self.cached.pop(0)


class Query(object):
    DEFAULT_FIELDS = ['_meta.kind', '_meta.key']

    def __init__(self, kind, *args, **kwargs):
        """

        :param amt.ext.rdb.model.Model cls: cls
        :param amt.ext.rdb.key.Key ancestor: ancestor
        :return:
        """
        self._cls = kind
        self._query_args = args
        self._ancestor = kwargs.get('ancestor')
        self._orders = kwargs.get('orders', {})
        self._limit = 100
        self._ctx = get_context()

    def __iter__(self):
        return QueryIterator(self)

    @classmethod
    def _prepare_query_args(cls, query_args):
        """

        :param list | tuple query_args: query args
        :rtype: dict
        """
        _data = {}
        for name, op, value in query_args:
            if op in Operations._RANGE_OPS:
                if name not in _data:
                    _data[name] = name, Operations.RANGE, {'$gt': None, '$gte': None, '$lt':  None, '$lte': None}
                # _data[name][2][Operations._RANGE_OPS_GROUPS[op]] = op
                _data[name][2][op] = value
            else:
                _data[name] = name, op, value
        return _data.values()

    def _build_search_query(self):
        _query_args = self._prepare_query_args(self._query_args)
        must, must_not, should = {}, {}, {}
        # if self._ancestor is not None:
        #     must["_meta.key"] = PrefixQuery("_meta.key", self._ancestor.redis_key + ':')

        for name, op, value in _query_args:
            if op == Operations.EQ:
                must[name] = value
            elif op == Operations.IN:
                must[name] = {"$in": value}
            elif op == Operations.NE:
                must_not[name] = {"$ne": value}
            elif op == Operations.MATCH:
                must[name] = {"$text": value}
            elif op == Operations.ISNULL:
                must[name] = {'$type': 10}
            # elif op == Operations.FUZZY:
            #     must[name] = FuzzyLikeThisFieldQuery(name, value)
            elif op == Operations.MISSING:
                must[name] = {'$exists': False}
            elif op == Operations.NOT_MISSING:
                must_not[name] = {'$exists': True}
            elif op == Operations.RANGE:
                must[name] = dict({(k, v) for (k,v) in value.items() if v is not None})
            # elif op == Operations.RANGE_NOT:
            #     must_not[name] = RangeQuery(ESRangeOp(name, value['g'], value['g_v'], value['l'], value['l_v']))
        if must or must_not or should:
            query = must
        else:
            query = {}
        return query

    def _build_search_body(self, limit=None):
        fields = set(self.DEFAULT_FIELDS + [c[0] for c in self._query_args] + [o.field for o in self._orders])
        query = self._build_search_query()
        s = Search(query=query, size=limit or self._limit, fields=list(fields))
        for order in self._orders:
            s.sort.add(order)
        try:
            logging.debug("Query: %s", json.dumps(s.serialize()))
        except Exception as e:
            pass
        return s.serialize()

    def order(self, *args):
        for arg in args:
            _arg = arg if isinstance(arg, tuple) else +arg
            self._orders[_arg[0]] = 1 if _arg[1] == 'asc' else -1
        return self

    def query(self, *args):
        self._query_args += args
        return self

    def _validate(self, redis_key):
        if self._ancestor is None:
            return len(redis_key.split(":")) == 2
        else:
            return len(redis_key.split(":")) == 4

    def get(self, keys_only=False, **kwargs):
        conn = self._ctx.get_mongo_collection(self._cls._get_kind())
        data = conn.find_one(self._build_search_query())
        if data:
            return self._ctx.build_entity(self._cls, Key.from_mongo_key(data.get('_id')), data)

    @property
    def query_raw(self):
        return self._build_search_body(100)

    def fetch(self, limit=100, skip=0, keys_only=False, fields=None):
        conn = self._ctx.get_mongo_collection(self._cls._get_kind())
        sort = self._orders.items()
        for data in conn.find(self._build_search_query(), limit=limit, skip=skip, sort=sort):
            if keys_only:
                yield Key.from_mongo_key(data.get('_id'))
            else:
                yield self._ctx.build_entity(self._cls, Key.from_mongo_key(data.get('_id')), data)

    def fetch_page(self, limit=100, cursor=None, keys_only=False, scroll_timeout='60s', fields=None):
        conn = self._ctx.elasticsearch
        query = self._build_search_body(limit)
        if fields:
            query['fields'] += fields
        start = time.time()
        if cursor is None:
            result = conn.search(body=query, scroll=scroll_timeout, index="rdb_" + self._cls._get_kind().lower(), doc_type="rdb_model", ignore_unavailable=True)
        else:
            result = conn.scroll(scroll_id=cursor, scroll=scroll_timeout)
        self._ctx.increase_elastic(time.time() - start)
        cursor = result.get('_scroll_id', None)

        if fields:
            values = result['hits']['hits']
            return [v['fields'] for v in fields], len(values) >= (limit or self._limit), cursor

        keys = []
        for v in result['hits']['hits']:
            keys.append(Key.from_redis_key(v['fields']['_meta.key'][0]))
        if keys_only:
            return keys, len(keys) >= (limit or self._limit), cursor
        return [k for k in self._ctx.get_multi(keys) if k is not None], len(keys) >= (limit or self._limit), cursor

    def count(self):
        """

        :rtype: int
        """
        conn = self._ctx.get_mongo_collection(self._cls._get_kind())
        return conn.find(self._build_search_query()).count()

    def aggregate(self, *aggs):
        conn = self._ctx.elasticsearch
        query = self._build_search_query()
        agg_factory = AggFactory()
        for agg in aggs:
            agg_factory.add(agg)
        query = Search(query=query, agg=agg_factory).serialize()
        start = time.time()
        result = conn.search(body=query, index="rdb_" + self._cls._get_kind().lower(), doc_type="rdb_model", ignore_unavailable=True, search_type='count')
        self._ctx.increase_elastic(time.time() - start)
        return result['aggregations']

    def map(self, callback, limit=None):
        conn = self._ctx.elasticsearch
        start = time.time()
        result = conn.search(body=self._build_search_body(limit), index="rdb_" + self._cls._get_kind().lower(), doc_type="rdb_model", ignore_unavailable=True)
        self._ctx.increase_elastic(time.time() - start)
        keys = []
        for v in result['hits']['hits']:
            keys.append(Key.from_redis_key(v['fields']['_meta.key'][0]))
        return [callback(k) for k in self._ctx.get_multi(keys) if k is not None]

    def iterate(self, **kwargs):
        return QueryIterator(self, **kwargs)