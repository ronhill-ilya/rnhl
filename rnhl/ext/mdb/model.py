
from .properties._property import Property
from .properties.structured import StructuredProperty
from .properties.model_key import ModelKey


from .key import Key, _validate_key
from .context import get_context
from .query import Query
import logging
from bson import ObjectId

all_models = {}

class MetaModel(type):
    def __new__(cls, name, bases, dct):
        new_cls = super(MetaModel, cls).__new__(cls, name, bases, dct)
        all_models[name] = new_cls
        return new_cls

    def __init__(cls, name, bases, classdict):
        super(MetaModel, cls).__init__(name, bases, classdict)
        cls._fix_up_properties()

    def __repr__(cls):
        return cls.__name__

class Model(object):
    __metaclass__ = MetaModel

    # Class variables
    _use_spots = False

    # Class variables updated by _fix_up_properties()
    _properties = None
    _code_name_properties = None
    _has_repeated = False
    _kind_map = {}  # Dict mapping {kind: Model subclass}

    # Defaults for instance variables.
    _entity_key = None
    _values = None
    _projection = ()  # Tuple of names of projected properties.

    # # Hardcoded pseudo-property for the key.
    _key = ModelKey()
    key = _key

    def __init__(self, id=None, key=None, parent=None, **kwargs):
        if key is not None:
            if id is not None:
                raise ValueError('Model constructor given key= does not accept id=')
            self._key = _validate_key(key, entity=self)
            self._changes = {}
        elif id is not None:
            self._key = Key(self.__class__, id, parent=parent)
        self._values = {}
        self._set_attributes(kwargs)

    def _set_attributes(self, kwds):
        cls = self.__class__
        for name, value in kwds.iteritems():
            prop = getattr(cls, name)  # Raises AttributeError for unknown properties.
            if not isinstance(prop, Property):
                raise TypeError('Cannot set non-property %s' % name)
            prop._set_value(self, value)

    def populate(self, data):
        data.pop('id', None)
        cls = self.__class__
        for name, value in data.iteritems():
            prop = getattr(cls, name)  # Raises AttributeError for unknown properties.
            if not isinstance(prop, Property):
                raise TypeError('Cannot set non-property %s' % name)
            prop._populate_from_dict(self, value)

    def to_dict(self, include=None, exclude=None, **kwargs):
        data = self._to_dict(include, exclude)
        data['id'] = self.key.urlsafe()
        return data

    def _validate_key(self, key):
        return _validate_key(key, self)

    @classmethod
    def _fix_up_properties(cls):
        cls._properties = {}  # Map of {name: Property}
        cls._code_name_properties = {} # Map of {code_name: Property}
        if cls.__module__ == __name__:  # Skip the classes in *this* file.
            return
        for name in set(dir(cls)):
            attr = getattr(cls, name, None)
            if isinstance(attr, Property) and not isinstance(attr, ModelKey):
                if name.startswith('_') and not isinstance(attr, ModelKey):
                    raise TypeError('ModelAttribute %s cannot begin with an underscore '
                                    'character. _ prefixed attributes are reserved for '
                                    'temporary Model instance values.' % name)
                attr._fix_up(cls, name)
                cls._properties[attr._name] = attr
            if isinstance(attr, Property):
                if (attr._repeated or (isinstance(attr, StructuredProperty) and attr._modelclass._has_repeated)):
                    cls._has_repeated = True

        cls._update_kind_map()

    @classmethod
    def _update_kind_map(cls):
        """Update the kind map to include this class."""
        cls._kind_map[cls._get_kind()] = cls

    @classmethod
    def _get_kind(cls):
        """
        Return the kind name for this class.
        """
        return cls.__name__

    @classmethod
    def get_cls_by_kind(cls, kind):
        """

        :param str kind: kind
        :rtype: type(Model)
        """
        return cls._kind_map[kind]

    def __repr__(self):
        """
        Return an unambiguous string representation of an entity.
        """
        key_val = 'None'
        try:
            key_val = self.key.id()
        except:
            pass
        return '%s(%s)' % (self.__class__.__name__, key_val)

    @classmethod
    def _index_name(cls):
        return cls._get_kind().lower()

    @classmethod
    def _class_name(cls):
        return cls._get_kind()

    @classmethod
    def redis_key_allocated_ids(cls):
        return cls._get_kind() + ":_allocated_ids"

    @classmethod
    def redis_key_kind_children(cls):
        return cls._get_kind() + ":_children"

    @classmethod
    def get_or_insert(cls, id, **kwargs):
        ctx = get_context()
        return ctx.get_or_insert(Key(cls, id), **kwargs)

    @classmethod
    def _get_index_name(cls):
        return cls._get_kind().lower()


    @classmethod
    def get_by_id(cls, id, **kwargs):
        return Key(cls, id).get(**kwargs)

    def _to_dict(self, include=None, exclude=None):
        """Return a dict containing the entity's property values.

        Args:
            include: Optional set of property names to include, default all.
            exclude: Optional set of property names to skip, default none.
            A name contained in both include and exclude is excluded.
        """
        if (include is not None and not isinstance(include, (list, tuple, set, frozenset))):
            raise TypeError('include should be a list, tuple or set')
        if (exclude is not None and not isinstance(exclude, (list, tuple, set, frozenset))):
            raise TypeError('exclude should be a list, tuple or set')
        values = {}
        for prop in self._properties.itervalues():
            name = prop._code_name
            if include is not None and name not in include:
                continue
            if exclude is not None and name in exclude:
                continue
            values[name] = prop._get_for_dict(self)
        return values

    def _to_index(self, include=None, exclude=None):
        """Return a dict containing the entity's property values.

        Args:
            include: Optional set of property names to include, default all.
            exclude: Optional set of property names to skip, default none.
            A name contained in both include and exclude is excluded.
        """
        if (include is not None and not isinstance(include, (list, tuple, set, frozenset))):
            raise TypeError('include should be a list, tuple or set')
        if (exclude is not None and not isinstance(exclude, (list, tuple, set, frozenset))):
            raise TypeError('exclude should be a list, tuple or set')
        values = {}
        for prop in self._properties.itervalues():
            name = prop._code_name
            if include is not None and name not in include:
                continue
            if exclude is not None and name in exclude:
                continue
            values[name] = prop._get_for_index(self)
            if isinstance(values[name], (str, unicode)) and len(values[name]) > 32765:
                del values[name]
            if hasattr(prop, '_full_text') and prop._full_text:
                logging.debug("Has analised")
                values[name + "_analized_str"] = prop._get_for_index(self)
        return values

    def put(self, **ctx_options):
        ctx = get_context(**ctx_options)
        self._pre_put()
        if self._key is None:
            self._key = Key(self.__class__, ObjectId())

        ctx.put(self)
        return self._key

    def put_async(self, **ctx_options):
        return self.put(**ctx_options)

    def _prepare_for_put(self):
        if self._properties:
            for prop in self._properties.itervalues():
                prop._prepare_for_put(self)

    def __eq__(self, other):
        """Compare two entities of the same class for equality."""
        if other.__class__ is not self.__class__:
            return NotImplemented
        if self._key != other._key:
            # TODO: If one key is None and the other is an explicit
            # incomplete key of the simplest form, this should be OK.
            return False
        return True

    @classmethod
    def query(cls, *args, **kwargs):
        """
        :rtype: Query
        """
        return Query(cls, *args, **kwargs)

    @classmethod
    def _from_google_datastore_entity(cls, entity):
        """

        :param gcloud.datastore.entity.Entity entity: entity
        :return:
        """
        data = {}
        for key, value in entity.iteritems():
            path = data
            parts = key.split(".")
            for path_part in parts[:-1]:
                if path_part not in path:
                    path[path_part] = {}
                path = path[path_part]
            path[parts[-1]] = value

        flat_path = entity.key.flat_path if hasattr(entity, 'key') else entity['__key_path']
        ent = cls(key=Key.from_redis_key(Key.URLSAFE_SEPARATE.join([str(p) for p in flat_path])))
        for key, value in data.iteritems():
            prop = cls._properties.get(key)
            if prop is not None:
                prop._from_google_datastore_entity_value(ent, value)
        return ent

    @classmethod
    def _get_index_mapping(cls):
        pass

    def _pre_put(self):
        pass

    def _post_get(self):
        pass


