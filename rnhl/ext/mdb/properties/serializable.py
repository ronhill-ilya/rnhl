import json
import pickle
import base64
from .blob import BlobProperty
from ._property import Property
from . import _utils


class PickleProperty(BlobProperty):
    """A Property whose value is any picklable Python object."""

    def _to_base_type(self, value):
        return value

    def _from_base_type(self, value):
        return value

    def _to_index_defenition(self):
        return "%s %s%s" % (self._name, "bytea", "[]" if self._repeated else "")


class JsonProperty(Property):
    """A property whose value is any Json-encodable Python object."""

    _json_type = None

    @_utils.positional(1 + BlobProperty._positional)
    def __init__(self, name=None, json_type=dict, **kwds):
        super(JsonProperty, self).__init__(name=name, **kwds)
        self._json_type = json_type

    def _validate(self, value):
        if self._json_type is not None and not isinstance(value, self._json_type):
            raise TypeError('JSON property must be a %s' % self._json_type)

    # Use late import so the dependency is optional.

    def _to_base_type(self, value):
        return value

    def _from_base_type(self, value):
        return value

    def _db_set_value(self, value):
        if not isinstance(value, (dict)):
            raise TypeError('IntegerProperty %s can only be set to integer values; '
                            'received %r' % (self._name, value))
        return value

    def _db_get_value(self, value):
        if isinstance(value, (str, unicode)):
            value = base64.b64decode(value)
            value = json.loads(value)
        return dict(value)

    def _to_index_defenition(self):
        if self._repeated:
            return "%s %s%s" % (self._name, "bytea", "")
        else:
            return "%s %s%s" % (self._name, "json", "[]" if self._repeated else "")