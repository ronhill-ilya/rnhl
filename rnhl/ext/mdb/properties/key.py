from bson import json_util
from ._property import Property, _BaseValue
from . import _utils
from ..model import Model
from ..key import Key


class KeyProperty(Property):
    """A Property whose value is a Key object.

    Optional keyword argument: kind=<kind>, to require that keys
    assigned to this property always have the indicated kind.  May be a
    string or a Model subclass.
    """

    _attributes = Property._attributes + ['_kind']

    _kind = None

    @_utils.positional(2 + Property._positional)
    def __init__(self, *args, **kwds):
        # Support several positional signatures:
        # ()  =>  name=None, kind from kwds
        # (None)  =>  name=None, kind from kwds
        # (name)  =>  name=arg 0, kind from kwds
        # (kind)  =>  name=None, kind=arg 0
        # (name, kind)  => name=arg 0, kind=arg 1
        # (kind, name)  => name=arg 1, kind=arg 0
        # The positional kind must be a Model subclass; it cannot be a string.
        name = kind = None

        for arg in args:
            if isinstance(arg, basestring):
                if name is not None:
                    raise TypeError('You can only specify one name')
                name = arg
            elif isinstance(arg, type) and issubclass(arg, Model):
                if kind is not None:
                    raise TypeError('You can only specify one kind')
                kind = arg
            elif arg is not None:
                raise TypeError('Unexpected positional argument: %r' % (arg,))

        if name is None:
            name = kwds.pop('name', None)
        elif 'name' in kwds:
            raise TypeError('You can only specify name once')

        if kind is None:
            kind = kwds.pop('kind', None)
        elif 'kind' in kwds:
            raise TypeError('You can only specify kind once')

        if kind is not None:
            if isinstance(kind, type) and issubclass(kind, Model):
                kind = kind._get_kind()
            if isinstance(kind, unicode):
                kind = kind.encode('utf-8')
            if not isinstance(kind, str):
                raise TypeError('kind must be a Model class or a string')

        super(KeyProperty, self).__init__(name, **kwds)

        self._kind = kind

    def _validate(self, value):
        if not isinstance(value, Key):
            raise ValueError('Expected Key, got %r' % (value,))
        if value.id() is None:
            raise ValueError('Expected complete Key, got %r' % (value,))
        if self._kind is not None:
            if value.kind() != self._kind:
                raise ValueError('Expected Key with kind=%r, got %r' % (self._kind, value))

    def _db_set_value(self, value):
        if not isinstance(value, Key):
            raise TypeError('KeyProperty %s can only be set to Key values; '
                            'received %r' % (self._name, value))
        # See datastore_types.PackKey
        return value.mongo_key

    def _db_get_value(self, value):
        return Key.from_mongo_key(value)

    def _get_for_dict(self, entity):
        val = self._get_value(entity)
        if val is not None:
            if self._repeated:
                return [v.urlsafe() for v in val]
            else:
                return val.urlsafe()

    def _populate_from_dict(self, entity, value):
        if self._repeated:
            value = [Key(urlsafe=v) for v in value]
        else:
            if value is None:
                return
            value = Key(urlsafe=value)
        self._set_value(entity, value)

    def _get_for_index(self, entity):
        val = self._get_value(entity)
        if val is not None:
            if self._repeated:
                return [v.mongo_key for v in val]
            else:
                return val.mongo_key

    def _datastore_type(self, value):
        return value.mongo_key

    def _to_index_defenition(self):
        return "%s %s%s" % (self._name, "varchar", "[]" if self._repeated else "")



