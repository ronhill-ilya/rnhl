import copy
import json
import logging

from ._property import Property, _BaseValue
from .model_key import ModelKey
from . import _utils
from .blob import BlobProperty


class _StructuredGetForDictMixin(Property):
    """Mixin class so *StructuredProperty can share _get_for_dict().

    The behavior here is that sub-entities are converted to dictionaries
    by calling to_dict() on them (also doing the right thing for
    repeated properties).

    NOTE: Even though the _validate() method in StructuredProperty and
    LocalStructuredProperty are identical, they cannot be moved into
    this shared base class.  The reason is subtle: _validate() is not a
    regular method, but treated specially by _call_to_base_type() and
    _call_shallow_validation(), and the class where it occurs matters
    if it also defines _to_base_type().
    """

    def _get_for_dict(self, entity):
        value = self._get_value(entity)
        if self._repeated:
            value = [v._to_dict() for v in value]
        elif value is not None:
            value = value._to_dict()
        return value


    def _get_for_index(self, entity):
        value = self._get_value(entity)
        if self._repeated:
            value = [v._to_index() for v in value]
        elif value is not None:
            value = value._to_index()
        return value



class StructuredProperty(_StructuredGetForDictMixin):
    """A Property whose value is itself an entity.

    The values of the sub-entity are indexed and can be queried.

    See the module docstring for details.
    """

    _modelclass = None

    _attributes = ['_modelclass'] + Property._attributes
    _positional = 1 + Property._positional  # Add modelclass as positional arg.

    @_utils.positional(1 + _positional)
    def __init__(self, modelclass, name=None, **kwds):
        super(StructuredProperty, self).__init__(name=name, **kwds)
        if self._repeated:
            if modelclass._has_repeated:
                raise TypeError('This StructuredProperty cannot use repeated=True '
                                'because its model class (%s) contains repeated '
                                'properties (directly or indirectly).' %
                                modelclass.__name__)
        self._modelclass = modelclass

    def _get_value(self, entity):
        """Override _get_value() to *not* raise UnprojectedPropertyError."""
        value = self._get_user_value(entity)
        if value is None and entity._projection:
            # Invoke super _get_value() to raise the proper exception.
            return super(StructuredProperty, self)._get_value(entity)
        return value

    def _populate_from_dict(self, entity, value):
        if self._repeated:
            _value = []
            for v in value:
                if v is not None:
                    m = self._modelclass()
                    m.populate(v)
                else:
                    m = v
                _value.append(m)
            value = _value
        else:
            value = self._modelclass().populate(value) if value is not None else value
        if value is not None and not (isinstance(value, list) and any([v is not None for v in value])):
            self._set_value(entity, value)

    def __getattr__(self, attrname):
        """Dynamically get a subproperty."""
        # Optimistically try to use the dict key.
        prop = self._modelclass._properties.get(attrname)
        # We're done if we have a hit and _code_name matches.
        if prop is None or prop._code_name != attrname:
            # Otherwise, use linear search looking for a matching _code_name.
            for prop in self._modelclass._properties.values():
                if prop._code_name == attrname:
                    break
            else:
                # This is executed when we never execute the above break.
                prop = None
        if prop is None:
            raise AttributeError('Model subclass %s has no attribute %s' %
                                 (self._modelclass.__name__, attrname))
        prop_copy = copy.copy(prop)
        prop_copy._code_name = self._code_name + '.' + prop_copy._code_name
        # Cache the outcome, so subsequent requests for the same attribute
        # name will get the copied property directly rather than going
        # through the above motions all over again.
        setattr(self, attrname, prop_copy)
        return prop_copy

    def _validate(self, value):
        if isinstance(value, dict):
            # A dict is assumed to be the result of a _to_dict() call.
            return self._modelclass(**value)
        if not isinstance(value, self._modelclass):
            raise ValueError('Expected %s instance, got %r' % (self._modelclass.__name__, value))

    def _has_value(self, entity, rest=None):
        # rest: optional list of attribute names to check in addition.
        # Basically, prop._has_value(self, ent, ['x', 'y']) is similar to
        # (prop._has_value(ent) and
        #    prop.x._has_value(ent.x) and
        #    prop.x.y._has_value(ent.x.y))
        # assuming prop.x and prop.x.y exist.
        # NOTE: This is not particularly efficient if len(rest) > 1,
        # but that seems a rare case, so for now I don't care.
        ok = super(StructuredProperty, self)._has_value(entity)
        if ok and rest:
            lst = self._get_base_value_unwrapped_as_list(entity)
            if len(lst) != 1:
                raise RuntimeError('Failed to retrieve sub-entity of StructuredProperty'
                                   ' %s' % self._name)
            subent = lst[0]
            if subent is None:
                return True
            subprop = subent._properties.get(rest[0])
            if subprop is None:
                ok = False
            else:
                ok = subprop._has_value(subent, rest[1:])
        return ok

    def _serialize(self, entity, depth=0):
        # entity -> pb; pb is an EntityProto message
        data = []
        values = self._get_base_value_unwrapped_as_list(entity)
        for value in values:
            if value is not None:
                tmp = {}
                for unused_name, prop in sorted(value._properties.iteritems()):
                    tmp[prop._name] = prop._serialize(value, depth + 1)
                data.append(tmp)
            else:
                # Serialize a single None
                # data.append(super(StructuredProperty, self)._serialize(entity, depth + 1))
                data.append(None)
        return data

    def _deserialize(self, entity, val, depth=0):
        data = val
        value = []
        for prop_val in data:
            if prop_val is not None:
                sub_entity = self._modelclass()
                for unused_name, prop in sorted(sub_entity._properties.iteritems()):
                    if prop_val.get(prop._name) is not None:
                        prop._deserialize(sub_entity, prop_val.get(prop._name), depth + 1)
                value.append(_BaseValue(sub_entity))
            else:
                value.append(prop_val)
        if self._repeated:
            self._store_value(entity, value)
        else:
            self._store_value(entity, value[0])

    def _prepare_for_put(self, entity):
        values = self._get_base_value_unwrapped_as_list(entity)
        for value in values:
            if value is not None:
                value._prepare_for_put()

    def _check_property(self, rest=None, require_indexed=True):
        """Override for Property._check_property().

        Raises:
          InvalidPropertyError if no subproperty is specified or if something
          is wrong with the subproperty.
        """
        if not rest:
            raise ValueError('Structured property %s requires a subproperty' % self._name)
        self._modelclass._check_properties([rest], require_indexed=require_indexed)

    def _get_base_value_at_index(self, entity, index):
        assert self._repeated
        value = self._retrieve_value(entity, self._default)
        value[index] = self._opt_call_to_base_type(value[index])
        return value[index].b_val

    def _get_value_size(self, entity):
        values = self._retrieve_value(entity, self._default)
        if values is None:
            return 0
        return len(values)

    def _to_index_defenition(self):
        if self._repeated:
            return "%s %s" % (self._name, 'bytea')
        properties = []
        for name, prop in sorted(self._modelclass._properties.iteritems()):
            if isinstance(prop, ModelKey):
                continue
            attr_prop = prop._to_index_defenition()
            if attr_prop is not None:
                properties.append("%s__%s" % (self._name, attr_prop))
        if properties:
            return ", ".join(properties)

class LocalStructuredProperty(StructuredProperty):
    pass
    # """Substructure that is serialized to an opaque blob.
    #
    # This looks like StructuredProperty on the Python side, but is
    # written like a BlobProperty in the datastore.  It is not indexed
    # and you cannot query for subproperties.  On the other hand, the
    # on-disk representation is more efficient and can be made even more
    # efficient by passing compressed=True, which compresses the blob
    # data using gzip.
    # """
    #
    # _indexed = False
    # _modelclass = None
    # _keep_keys = False
    #
    # _attributes = ['_modelclass'] + BlobProperty._attributes + ['_keep_keys']
    # _positional = 1 + BlobProperty._positional  # Add modelclass as positional.
    #
    # @_utils.positional(1 + _positional)
    # def __init__(self, modelclass,
    #              name=None, compressed=False, keep_keys=False,
    #              **kwds):
    #     super(LocalStructuredProperty, self).__init__(name=name,
    #                                                   compressed=compressed,
    #                                                   **kwds)
    #     if self._indexed:
    #         raise NotImplementedError('Cannot index LocalStructuredProperty %s.' %
    #                                   self._name)
    #     self._modelclass = modelclass
    #     self._keep_keys = keep_keys
    #
    # def _validate(self, value):
    #     if isinstance(value, dict):
    #         # A dict is assumed to be the result of a _to_dict() call.
    #         return self._modelclass(**value)
    #     if not isinstance(value, self._modelclass):
    #         raise ValueError('Expected %s instance, got %r' % (self._modelclass.__name__, value))
    #
    # def _to_base_type(self, value):
    #     if isinstance(value, self._modelclass):
    #         pb = value._to_pb(set_key=self._keep_keys)
    #         return pb.SerializePartialToString()
    #
    # def _from_base_type(self, value):
    #     if not isinstance(value, self._modelclass):
    #         pb = entity_pb.EntityProto()
    #         pb.MergePartialFromString(value)
    #         if not self._keep_keys:
    #             pb.clear_key()
    #         return self._modelclass._from_pb(pb)
    #
    # def _prepare_for_put(self, entity):
    #     # TODO: Using _get_user_value() here makes it impossible to
    #     # subclass this class and add a _from_base_type().  But using
    #     # _get_base_value() won't work, since that would return
    #     # the serialized (and possibly compressed) serialized blob.
    #     value = self._get_user_value(entity)
    #     if value is not None:
    #         if self._repeated:
    #             for subent in value:
    #                 if subent is not None:
    #                     subent._prepare_for_put()
    #         else:
    #             value._prepare_for_put()
    #
    # def _db_set_uncompressed_meaning(self, p):
    #     p.set_meaning(entity_pb.Property.ENTITY_PROTO)