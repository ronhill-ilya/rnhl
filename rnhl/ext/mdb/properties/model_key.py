from ._property import Property
from . import _utils
from ..key import _validate_key, Key


class ModelKey(Property):
    """Special property to store the Model key."""

    def __init__(self):
        super(ModelKey, self).__init__()
        self._name = '_id'

    def _datastore_type(self, value):
        return value.mongo_key

    def _validate(self, value):
        return _validate_key(value)

    def _set_value(self, entity, value):
        """Setter for key attribute."""
        if value is not None:
            value = _validate_key(value, entity=entity)
            value = entity._validate_key(value)
        entity._entity_key = value

    def __set__(self, entity, value):
        """
        Descriptor protocol: set the value on the entity.
        """
        self._set_value(entity, value)

    def _get_value(self, entity):
        """Getter for key attribute."""
        return entity._entity_key

    def _delete_value(self, entity):
        """Deleter for key attribute."""
        entity._entity_key = None

    def _get_for_dict(self, entity):
        val = self._get_value(entity)
        if val is not None:
            return val.mongo_key

    def _to_index_defenition(self):
        return "%s %s" % (self._name, "varchar primary key")

    def __pos__(self):
        return "_id.id", 'ask'

    def __neg__(self):
        return "_id.id", 'desk'