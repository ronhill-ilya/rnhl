import zlib
import base64

from ._property import Property, _NotEqualMixin
from . import _utils


_MEANING_URI_COMPRESSED = 'ZLIB'
_MAX_STRING_LENGTH = 500


class _CompressedValue(_NotEqualMixin):
    """A marker object wrapping compressed values."""

    __slots__ = ['z_val']

    def __init__(self, z_val):
        """Constructor.  Argument is a string returned by zlib.compress()."""
        assert isinstance(z_val, str), repr(z_val)
        self.z_val = z_val

    def __repr__(self):
        return '_CompressedValue(%s)' % repr(self.z_val)

    def __eq__(self, other):
        if not isinstance(other, _CompressedValue):
            return NotImplemented
        return self.z_val == other.z_val

    def __hash__(self):
        raise TypeError('_CompressedValue is not immutable')


class BlobProperty(Property):
    """A Property whose value is a byte string.  It may be compressed."""

    _indexed = False
    _compressed = False

    _attributes = Property._attributes + ['_compressed']

    @_utils.positional(1 + Property._positional)
    def __init__(self, name=None, compressed=False, **kwds):
        super(BlobProperty, self).__init__(name=name, **kwds)
        self._compressed = compressed
        if compressed and self._indexed:
            # TODO: Allow this, but only allow == and IN comparisons?
            raise NotImplementedError('BlobProperty %s cannot be compressed and '
                                      'indexed at the same time.' % self._name)

    def _value_to_repr(self, value):
        long_repr = super(BlobProperty, self)._value_to_repr(value)
        # Note that we may truncate even if the value is shorter than
        # _MAX_STRING_LENGTH; e.g. if it contains many \xXX or \uUUUU
        # escapes.
        if len(long_repr) > _MAX_STRING_LENGTH + 4:
            # Truncate, assuming the final character is the closing quote.
            long_repr = long_repr[:_MAX_STRING_LENGTH] + '...' + long_repr[-1]
        return long_repr

    # def _validate(self, value):
    #     if not isinstance(value, str):
    #         raise ValueError('Expected str, got %r' % (value,))

    def _to_base_type(self, value):
        if self._compressed:
            return _CompressedValue(zlib.compress(value))
        return value

    def _from_base_type(self, value):
        if isinstance(value, _CompressedValue):
            return zlib.decompress(value.z_val)
        return value

    def _db_set_value(self, value):
        if isinstance(value, _CompressedValue):
            value = value.z_val
        return base64.b64encode(value)

    def _db_get_value(self, value):
        value = base64.b64decode(value)
        if self._compressed:
            value = _CompressedValue(value)
        return value

    def _get_for_index(self, entity):
        pass