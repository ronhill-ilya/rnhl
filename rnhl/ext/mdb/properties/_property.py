import _utils
from bson import json_util
from ..query import Operations as QueryOp


class ModelAttribute(object):
    def _fix_up(self, cls, code_name):
        pass


class _NotEqualMixin(object):
    """Mix-in class that implements __ne__ in terms of __eq__."""

    def __ne__(self, other):
        """Implement self != other as not(self == other)."""
        eq = self.__eq__(other)
        if eq is NotImplemented:
            return NotImplemented
        return not eq


class _BaseValue(_NotEqualMixin):
    """A marker object wrapping a 'base type' value.

    This is used to be able to tell whether ent._values[name] is a
    user value (i.e. of a type that the Python code understands) or a
    base value (i.e of a type that serialization understands).
    User values are unwrapped; base values are wrapped in a
    _BaseValue instance.
    """

    __slots__ = ['b_val']

    def __init__(self, b_val):
        """Constructor.  Argument is the base value to be wrapped."""
        assert b_val is not None
        assert not isinstance(b_val, list), repr(b_val)
        self.b_val = b_val

    def __repr__(self):
        return '_BaseValue(%r)' % (self.b_val,)

    def __eq__(self, other):
        if not isinstance(other, _BaseValue):
            return NotImplemented
        return self.b_val == other.b_val

    def __hash__(self):
        raise TypeError('_BaseValue is not immutable')


class Property(ModelAttribute):
    _code_name = None
    _name = None
    _indexed = True
    _repeated = False
    _required = False
    _default = None
    _choices = None
    _validator = None
    _verbose_name = None

    __creation_counter_global = 0

    _attributes = ['_name', '_indexed', '_repeated', '_required', '_default',
                   '_choices', '_validator', '_verbose_name']
    _positional = 1  # Only name is a positional argument.

    @_utils.positional(1 + _positional)  # Add 1 for self.
    def __init__(self, name=None, indexed=None, repeated=None,
                 required=None, default=None, choices=None, validator=None,
                 verbose_name=None):
        """Constructor.  For arguments see the module docstring."""
        if name is not None:
            if isinstance(name, unicode):
                name = name.encode('utf-8')
            if not isinstance(name, str):
                raise TypeError('Name %r is not a string' % (name,))
            if '.' in name:
                raise ValueError('Name %r cannot contain period characters' % (name,))
            self._name = name
        if indexed is not None:
            self._indexed = indexed
        if repeated is not None:
            self._repeated = repeated
        if required is not None:
            self._required = required
        if default is not None:
            # TODO: Call _validate() on default?
            self._default = default
        if verbose_name is not None:
            self._verbose_name = verbose_name
        if self._repeated and (self._required or self._default is not None):
            raise ValueError('repeated is incompatible with required or default')
        if choices is not None:
            if not isinstance(choices, (list, tuple, set, frozenset)):
                raise TypeError('choices must be a list, tuple or set; received %r' %
                                choices)
            # TODO: Call _validate() on each choice?
            self._choices = frozenset(choices)
        if validator is not None:
            # The validator is called as follows:
            # value = validator(prop, value)
            # It should return the value to be used, or raise an exception.
            # It should be idempotent, i.e. calling it a second time should
            # not further modify the value.  So a validator that returns e.g.
            # value.lower() or value.strip() is fine, but one that returns
            # value + '$' is not.
            if not hasattr(validator, '__call__'):
                raise TypeError('validator must be callable or None; received %r' %
                                validator)
            self._validator = validator
        # Keep a unique creation counter.
        Property.__creation_counter_global += 1
        self._creation_counter = Property.__creation_counter_global

    def __repr__(self):
        """Return a compact unambiguous string representation of a property."""
        args = []
        cls = self.__class__
        for i, attr in enumerate(self._attributes):
            val = getattr(self, attr)
            if val is not getattr(cls, attr):
                if isinstance(val, type):
                    s = val.__name__
                else:
                    s = repr(val)
                if i >= cls._positional:
                    if attr.startswith('_'):
                        attr = attr[1:]
                    s = '%s=%s' % (attr, s)
                args.append(s)
        s = '%s(%s)' % (self.__class__.__name__, ', '.join(args))
        return s

    def _datastore_type(self, value):
        """Internal hook used by property filters.

        Sometimes the low-level query interface needs a specific data type
        in order for the right filter to be constructed.  See _comparison().
        """
        return value

    def _comparison(self, op, value):
        if not self._indexed:
            raise ValueError('Cannot query for unindexed property %s' % self._name)

        if value is not None:
            value = self._do_validate(value)
            value = self._call_to_base_type(value)
            value = self._datastore_type(value)
        else:
            if op == QueryOp.EQ:
                op = QueryOp.MISSING
            elif op == QueryOp.NE:
                op = QueryOp.NOT_MISSING
            elif op == QueryOp.ISNULL:
                op = QueryOp.ISNULL
            else:
                raise ValueError('"None" value do not support comparison with type %r' % op)

        return self._code_name, op, value


    def __eq__(self, value):
        """Return a FilterNode instance representing the '=' comparison."""
        return self._comparison(QueryOp.EQ, value)


    def __ne__(self, value):
        """Return a FilterNode instance representing the '!=' comparison."""
        return self._comparison(QueryOp.NE, value)


    def __lt__(self, value):
        """Return a FilterNode instance representing the '<' comparison."""
        return self._comparison(QueryOp.LT, value)


    def __le__(self, value):
        """Return a FilterNode instance representing the '<=' comparison."""
        return self._comparison(QueryOp.LTE, value)


    def __gt__(self, value):
        """Return a FilterNode instance representing the '>' comparison."""
        return self._comparison(QueryOp.GT, value)


    def __ge__(self, value):
        """Return a FilterNode instance representing the '>=' comparison."""
        return self._comparison(QueryOp.GTE, value)


    def is_none(self):
        return self._comparison(QueryOp.ISNULL, None)

    def is_not_none(self):
        return self._comparison(QueryOp.NE, {'$type': 10})


    def _IN(self, value):
        """Comparison operator for the 'in' comparison operator.

        The Python 'in' operator cannot be overloaded in the way we want
        to, so we define a method.  For example:

          Employee.query(Employee.rank.IN([4, 5, 6]))

        Note that the method is called ._IN() but may normally be invoked
        as .IN(); ._IN() is provided for the case you have a
        StructuredProperty with a model that has a Property named IN.
        """
        values = []
        for val in value:
            if val is not None:
                val = self._do_validate(val)
                val = self._call_to_base_type(val)
                val = self._datastore_type(val)
            values.append(val)
        return self._code_name, QueryOp.IN, values

    IN = _IN

    def __neg__(self):
        """Return a descending sort order on this Property.

        For example:

          Employee.query().order(-Employee.rank)
        """
        return self._name, 'desc'

    def __pos__(self):
        """Return an ascending sort order on this Property.

        Note that this is redundant but provided for consistency with
        __neg__.  For example, the following two are equivalent:

          Employee.query().order(+Employee.rank)
          Employee.query().order(Employee.rank)
        """
        return self._name, 'asc'

    def _do_validate(self, value):
        """Call all validations on the value.

        This calls the most derived _validate() method(s), then the custom
        validator function, and then checks the choices.  It returns the
        value, possibly modified in an idempotent way, or raises an
        exception.

        Note that this does not call all composable _validate() methods.
        It only calls _validate() methods up to but not including the
        first _to_base_type() method, when the MRO is traversed looking
        for _validate() and _to_base_type() methods.  (IOW if a class
        defines both _validate() and _to_base_type(), its _validate()
        is called and then the search is aborted.)

        Note that for a repeated Property this function should be called
        for each item in the list, not for the list as a whole.
        """
        if isinstance(value, _BaseValue):
            return value
        value = self._call_shallow_validation(value)
        if self._validator is not None:
            newvalue = self._validator(self, value)
            if newvalue is not None:
                value = newvalue
        if self._choices is not None:
            if value not in self._choices:
                raise ValueError(
                    'Value %r for property %s is not an allowed choice' %
                    (value, self._name))
        return value


    def _fix_up(self, cls, code_name):
        """Internal helper called to tell the property its name.

        This is called by _fix_up_properties() which is called by
        MetaModel when finishing the construction of a Model subclass.
        The name passed in is the name of the class attribute to which the
        Property is assigned (a.k.a. the code name).  Note that this means
        that each Property instance must be assigned to (at most) one
        class attribute.  E.g. to declare three strings, you must call
        StringProperty() three times, you cannot write

          foo = bar = baz = StringProperty()
        """
        self._code_name = code_name
        if self._name is None:
            self._name = code_name


    def _store_value(self, entity, value):
        """Internal helper to store a value in an entity for a Property.

        This assumes validation has already taken place.  For a repeated
        Property the value should be a list.
        """
        entity._values[self._name] = value


    def _set_value(self, entity, value):
        """Internal helper to set a value in an entity for a Property.

        This performs validation first.  For a repeated Property the value
        should be a list.
        """
        # if entity._projection:
        # raise A(
        # 'You cannot set property values of a projection entity')
        if self._repeated:
            if not isinstance(value, (list, tuple, set, frozenset)):
                raise ValueError('Expected list or tuple, got %r' % (value,))
            value = [self._do_validate(v) for v in value]
        else:
            if value is not None:
                value = self._do_validate(value)
        self._store_value(entity, value)


    def _has_value(self, entity, unused_rest=None):
        """Internal helper to ask if the entity has a value for this Property."""
        return self._name in entity._values


    def _retrieve_value(self, entity, default=None):
        """Internal helper to retrieve the value for this Property from an entity.

        This returns None if no value is set, or the default argument if
        given.  For a repeated Property this returns a list if a value is
        set, otherwise None.  No additional transformations are applied.
        """
        return entity._values.get(self._name, default)


    def _get_user_value(self, entity):
        """Return the user value for this property of the given entity.

        This implies removing the _BaseValue() wrapper if present, and
        if it is, calling all _from_base_type() methods, in the reverse
        method resolution order of the property's class.  It also handles
        default values and repeated properties.
        """
        return self._apply_to_values(entity, self._opt_call_from_base_type)


    def _get_base_value(self, entity):
        """Return the base value for this property of the given entity.

        This implies calling all _to_base_type() methods, in the method
        resolution order of the property's class, and adding a
        _BaseValue() wrapper, if one is not already present.  (If one
        is present, no work is done.)  It also handles default values and
        repeated properties.
        """
        return self._apply_to_values(entity, self._opt_call_to_base_type)


    # TODO: Invent a shorter name for this.
    def _get_base_value_unwrapped_as_list(self, entity):
        """Like _get_base_value(), but always returns a list.

        Returns:
          A new list of unwrapped base values.  For an unrepeated
          property, if the value is missing or None, returns [None]; for a
          repeated property, if the original value is missing or None or
          empty, returns [].
        """
        wrapped = self._get_base_value(entity)
        if self._repeated:
            if wrapped is None:
                return []
            assert isinstance(wrapped, list)
            return [w.b_val for w in wrapped]
        else:
            if wrapped is None:
                return [None]
            assert isinstance(wrapped, _BaseValue)
            return [wrapped.b_val]


    def _opt_call_from_base_type(self, value):
        """Call _from_base_type() if necessary.

        If the value is a _BaseValue instance, unwrap it and call all
        _from_base_type() methods.  Otherwise, return the value
        unchanged.
        """
        if isinstance(value, _BaseValue):
            value = self._call_from_base_type(value.b_val)
        return value


    def _value_to_repr(self, value):
        """Turn a value (base or not) into its repr().

        This exists so that property classes can override it separately.
        """
        # Manually apply _from_base_type() so as not to have a side
        # effect on what's contained in the entity.  Printing a value
        # should not change it!
        val = self._opt_call_from_base_type(value)
        return repr(val)


    def _opt_call_to_base_type(self, value):
        """Call _to_base_type() if necessary.

        If the value is a _BaseValue instance, return it unchanged.
        Otherwise, call all _validate() and _to_base_type() methods and
        wrap it in a _BaseValue instance.
        """
        if not isinstance(value, _BaseValue):
            value = _BaseValue(self._call_to_base_type(value))
        return value


    def _call_from_base_type(self, value):
        """Call all _from_base_type() methods on the value.

        This calls the methods in the reverse method resolution order of
        the property's class.
        """
        methods = self._find_methods('_from_base_type', reverse=True)
        call = self._apply_list(methods)
        return call(value)


    def _call_to_base_type(self, value):
        """Call all _validate() and _to_base_type() methods on the value.

        This calls the methods in the method resolution order of the
        property's class.
        """
        methods = self._find_methods('_validate', '_to_base_type')
        call = self._apply_list(methods)
        return call(value)


    def _call_shallow_validation(self, value):
        """Call the initial set of _validate() methods.

        This is similar to _call_to_base_type() except it only calls
        those _validate() methods that can be called without needing to
        call _to_base_type().

        An example: suppose the class hierarchy is A -> B -> C ->
        Property, and suppose A defines _validate() only, but B and C
        define _validate() and _to_base_type().  The full list of
        methods called by _call_to_base_type() is:

          A._validate()
          B._validate()
          B._to_base_type()
          C._validate()
          C._to_base_type()

        This method will call A._validate() and B._validate() but not the
        others.
        """
        methods = []
        for method in self._find_methods('_validate', '_to_base_type'):
            if method.__name__ != '_validate':
                break
            methods.append(method)
        call = self._apply_list(methods)
        return call(value)


    @classmethod
    def _find_methods(cls, *names, **kwds):
        """Compute a list of composable methods.

        Because this is a common operation and the class hierarchy is
        static, the outcome is cached (assuming that for a particular list
        of names the reversed flag is either always on, or always off).

        Args:
          *names: One or more method names.
          reverse: Optional flag, default False; if True, the list is
            reversed.

        Returns:
          A list of callable class method objects.
        """
        reverse = kwds.pop('reverse', False)
        assert not kwds, repr(kwds)
        cache = cls.__dict__.get('_find_methods_cache')
        if cache:
            hit = cache.get(names)
            if hit is not None:
                return hit
        else:
            cls._find_methods_cache = cache = {}
        methods = []
        for c in cls.__mro__:
            for name in names:
                method = c.__dict__.get(name)
                if method is not None:
                    methods.append(method)
        if reverse:
            methods.reverse()
        cache[names] = methods
        return methods


    def _apply_list(self, methods):
        """Return a single callable that applies a list of methods to a value.

        If a method returns None, the last value is kept; if it returns
        some other value, that replaces the last value.  Exceptions are
        not caught.
        """

        def call(value):
            for method in methods:
                newvalue = method(self, value)
                if newvalue is not None:
                    value = newvalue
            return value

        return call


    def _apply_to_values(self, entity, function):
        """Apply a function to the property value/values of a given entity.

        This retrieves the property value, applies the function, and then
        stores the value back.  For a repeated property, the function is
        applied separately to each of the values in the list.  The
        resulting value or list of values is both stored back in the
        entity and returned from this method.
        """
        value = self._retrieve_value(entity, self._default)
        if self._repeated:
            if value is None:
                value = []
                self._store_value(entity, value)
            else:
                value[:] = map(function, value)
        else:
            if value is not None:
                newvalue = function(value)
                if newvalue is not None and newvalue is not value:
                    self._store_value(entity, newvalue)
                    value = newvalue
        return value


    def _get_value(self, entity):
        """Internal helper to get the value for this Property from an entity.

        For a repeated Property this initializes the value to an empty
        list if it is not set.
        """
        # if entity._projection:
        # if self._name not in entity._projection:
        # raise UnprojectedPropertyError(
        # 'Property %s is not in the projection' % (self._name,))
        return self._get_user_value(entity)


    def _delete_value(self, entity):
        """Internal helper to delete the value for this Property from an entity.

        Note that if no value exists this is a no-op; deleted values will
        not be serialized but requesting their value will return None (or
        an empty list in the case of a repeated Property).
        """
        if self._name in entity._values:
            del entity._values[self._name]


    def _is_initialized(self, entity):
        """Internal helper to ask if the entity has a value for this Property.

        This returns False if a value is stored but it is None.
        """
        return (not self._required or
                ((self._has_value(entity) or self._default is not None) and
                 self._get_value(entity) is not None))


    def __get__(self, entity, unused_cls=None):
        """Descriptor protocol: get the value from the entity."""
        if entity is None:
            return self  # __get__ called on class
        return self._get_value(entity)


    def __set__(self, entity, value):
        """Descriptor protocol: set the value on the entity."""
        self._set_value(entity, value)


    def __delete__(self, entity):
        """Descriptor protocol: delete the value from the entity."""
        self._delete_value(entity)


    def _db_path(self, kind, id):
        return ":".join((kind, str(id), self._name))


    def _to_base_type(self, value):
        return value


    def _from_base_type(self, value):
        return value


    def _serialize(self, entity, depth=0):
        res = []
        for v in self._get_base_value_unwrapped_as_list(entity):
            res.append(self._db_set_value(v) if v is not None else v)
        return res

    def _deserialize(self, entity, val, depth=0):
        """Internal helper to deserialize this property from redis.

        Subclasses may override this method.

        Args:
          entity: The entity, a Model (subclass) instance.
          val: A Property Message object (a protocol buffer).
          !!!! depth: Optional nesting depth, default 1 (unused here, but used
            by some subclasses that override this method).
        """
        data = val
        val = []

        for v in data:
            val.append(self._db_get_value(v) if v is not None else v)
        val = [None if v == 'None' else v for v in val]
        if self._repeated:
            if isinstance(val, list):
                value = [(_BaseValue(v) if v is not None else None) for v in val]
            elif val is not None:
                value = [val]
            else:
                value = []
        else:
            if val is not None:
                val = val[0]
            value = _BaseValue(val) if val is not None else val
        self._store_value(entity, value)


    def _prepare_for_put(self, entity):
        pass


    def _check_property(self, rest=None, require_indexed=True):
        """Internal helper to check this property for specific requirements.

        Called by Model._check_properties().

        Args:
          rest: Optional subproperty to check, of the form 'name1.name2...nameN'.

        Raises:
          InvalidPropertyError if this property does not meet the given
          requirements or if a subproperty is specified.  (StructuredProperty
          overrides this method to handle subproperties.)
        """
        if require_indexed and not self._indexed:
            raise InvalidPropertyError('Property is unindexed %s' % self._name)
        if rest:
            raise InvalidPropertyError('Referencing subproperty %s.%s '
                                       'but %s is not a structured property' %
                                       (self._name, rest, self._name))

    def _get_for_dict(self, entity):
        """Retrieve the value like _get_value(), processed for _to_dict().

        Property subclasses can override this if they want the dictionary
        returned by entity._to_dict() to contain a different value.  The
        main use case is StructuredProperty and LocalStructuredProperty.

        NOTES:

        - If you override _get_for_dict() to return a different type, you
          must override _validate() to accept values of that type and
          convert them back to the original type.

        - If you override _get_for_dict(), you must handle repeated values
          and None correctly.  (See _StructuredGetForDictMixin for an
          example.)  However, _validate() does not need to handle these.
        """
        val = self._get_value(entity)
        if val is not None:
            if self._repeated:
                try:
                    return [json_util.default(v) for v in val]
                except TypeError:
                    return val
            else:
                try:
                    return json_util.default(val)
                except TypeError:
                    return val

    def _populate_from_dict(self, entity, value):
        if self._repeated:
            value = [json_util.object_hook(v) if isinstance(v, dict) else v for v in value]
        else:
            value = json_util.object_hook(value) if isinstance(value, dict) else value
        self._set_value(entity, value)

    def _get_for_index(self, entity):
        return self._get_value(entity)

    def _get_redis_key(self, key_path):
        return key_path + ":" + self._name

    def _to_index_mapping(self, mapping):
        raise NotImplementedError()

    def _to_index_defenition(self):
        return

    def _from_google_datastore_entity_value(self, entity, value):
        val = value if isinstance(value, list) else [value]
        if self._repeated:
            if isinstance(val, list):
                value = [(_BaseValue(v) if v is not None else None) for v in val]
            elif val is not None:
                value = [val]
            else:
                value = []
        else:
            if val is not None:
                val = val[0]
            value = _BaseValue(val) if val is not None else val
        if value is None or (isinstance(value, list) and all([v is None for v in value])):
            return
        self._store_value(entity, value)