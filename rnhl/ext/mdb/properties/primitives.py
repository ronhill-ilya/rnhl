from ._property import Property
from . import _utils


class IntegerProperty(Property):
    """A Property whose value is a Python int or long (or bool)."""

    def _validate(self, value):
        if not isinstance(value, (int, long)):
            raise ValueError('Expected integer, got %r' % (value,))
        return int(value)

    def _db_set_value(self, value):
        if not isinstance(value, (bool, int, long)):
            raise TypeError('IntegerProperty %s can only be set to integer values; '
                            'received %r' % (self._name, value))
        return value

    def _db_get_value(self, value):
        return int(value)



class FloatProperty(Property):
    """A Property whose value is a Python float.

    Note: int, long and bool are also allowed.
    """

    def _db_set_value(self, value):
        if not isinstance(value, (bool, int, long, float)):
            raise TypeError('FloatProperty %s can only be set to integer or float '
                            'values; received %r' % (self._name, value))
        return float(value)

    def _db_get_value(self, value):
        return float(value)

    def _to_index_mapping(self, mapping):
        mapping.add_property(FloatField(name=self._name, store="yes", index="analyzed"))

    def _to_index_defenition(self):
        return "%s %s%s" % (self._name, "real", "[]" if self._repeated else "")


class BooleanProperty(Property):
    """A Property whose value is a Python bool."""
    # TODO: Allow int/long values equal to 0 or 1?

    def _validate(self, value):
        if not isinstance(value, bool):
            raise ValueError('Expected bool, got %r' % (value,))
        return value

    def _db_set_value(self, value):
        if not isinstance(value, bool):
            raise TypeError('BooleanProperty %s can only be set to bool values; '
                            'received %r' % (self._name, value))
        return value

    def _db_get_value(self, value):
        return value

    def _to_index_mapping(self, mapping):
        mapping.add_property(BooleanField(name=self._name, store=True, index="analyzed"))

    def _to_index_defenition(self):
        return "%s %s%s" % (self._name, "boolean", "[]" if self._repeated else "")

