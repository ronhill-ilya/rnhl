from ._property import Property
from . import _utils
from flask import Blueprint, make_response, abort, url_for, request, jsonify
from bson import ObjectId

from gridfs import errors


class Image(object):
    PNG = 'image/png'
    JPEG = 'image/jpeg'
    GIF = 'image/gif'

    def __init__(self, id=None, size=None, content_type=None):
        self._id = id
        self._content_type = content_type
        self._size = size
        from rnhl.ext.mdb import get_context
        self._grid_fs = get_context().get_gridfs()

    def is_valid(self):
        return self._id and self._content_type and self._size

    def put(self, data, content_type=JPEG):
        self._id = self._grid_fs.put(data, content_type=content_type)
        self._content_type = content_type
        if isinstance(data, basestring):
            self._size = len(data)
        elif isinstance(data, file):
            self._size = data.tell()

    def get(self):
        if self._id is None:
            raise RuntimeError("id not found")
        return self._grid_fs.get(self._id)

    @property
    def serve_url(self):
        return url_for("mdb_images.serve_image", oid=str(self._id))

    def get_for_dict(self):
        return {
            "$image": {
                "oid": str(self._id),
                "src": self.serve_url,
                "size": self._size,
                "content_type": self._content_type
            }
        }

    @classmethod
    def from_dict(cls, data):
        if isinstance(data, dict):
            img_data = data["$image"]
            return cls(ObjectId(img_data['oid']), img_data['size'], img_data['content_type'])

class ImageProperty(Property):
    """A Property whose value GrifFS Image object"""
    _attributes = Property._attributes + ['_thumbnail_size']

    @_utils.positional(1 + Property._positional)
    def __init__(self, thumbnail_size=False, **kwds):
        super(ImageProperty, self).__init__(**kwds)
        self._thumbnail_size = thumbnail_size

    def _validate(self, value):
        if not isinstance(value, Image):
            raise ValueError('Expected Image, got %r' % (value,))

        # if not value.is_valid():
        #     raise ValueError('Image, not valid')

    def _db_set_value(self, value):
        """
        :param Image value: value
        """
        return {
            "id": value._id,
            "content_type": value._content_type,
            "size": value._size,
        }

    def _db_get_value(self, value):
        return Image(**value)

    def _get_for_dict(self, entity):
        val = self._get_value(entity)
        if val is not None:
            if self._repeated:
                return [v.get_for_dict() for v in val]
            else:
                return val.get_for_dict()

    def _populate_from_dict(self, entity, value):
        if self._repeated:
            value = [Image.from_dict(v) for v in value]
        else:
            value = Image.from_dict(value)
        self._set_value(entity, value)

image_serve = Blueprint('mdb_images', 'mdb_images', url_prefix="/_ah/mdb")

@image_serve.route('/images/<oid>', endpoint="serve_image")
def serve_grid_fs_image(oid):
    from rnhl.ext.mdb import get_context
    _grid_fs = get_context().get_gridfs()
    try:
        file = _grid_fs.get(ObjectId(oid))
        response = make_response(file.read())
        response.mimetype = file.content_type
        return response
    except errors.NoFile:
        abort(404)


@image_serve.route('/images', methods=['POST'])
def save_image():
    imq = Image()
    imq.put(request.files['file'])
    return jsonify(imq.get_for_dict())