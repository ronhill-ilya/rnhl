
from ._property import Property
from .blob import BlobProperty, _MAX_STRING_LENGTH
from . import _utils
from ..query import Operations

class TextProperty(BlobProperty):
    """An unindexed Property whose value is a text string of unlimited length."""

    _attributes = BlobProperty._attributes + ['_full_text']

    @_utils.positional(1 + BlobProperty._positional)
    def __init__(self, full_text=False, **kwds):
        super(TextProperty, self).__init__(**kwds)
        self._full_text = full_text

    def _validate(self, value):
        if isinstance(value, str):
            # Decode from UTF-8 -- if this fails, we can't write it.
            try:
                value = unicode(value, 'utf-8')
            except UnicodeError:
                raise ValueError('Expected valid UTF-8, got %r' % (value,))
        elif not isinstance(value, unicode):
            raise ValueError('Expected string, got %r' % (value,))
        if self._indexed and len(value) > _MAX_STRING_LENGTH:
            raise ValueError('Indexed value %s must be at most %d characters' % (self._name, _MAX_STRING_LENGTH))

    def _to_base_type(self, value):
        if isinstance(value, unicode):
            return value.encode('utf-8')

    def _from_base_type(self, value):
        if isinstance(value, str):
            try:
                return unicode(value, 'utf-8')
            except UnicodeDecodeError:
                # Since older versions of NDB could write non-UTF-8 TEXT
                # properties, we can't just reject these.  But _validate() now
                # rejects these, so you can't write new non-UTF-8 TEXT
                # properties.
                # TODO: Eventually we should close this hole.
                pass

    def _to_index_mapping(self, mapping):
        mapping.add_property(StringField(name=self._name, store=True, index="analyzed"))

    def _to_index_defenition(self):
        return "%s %s" % (self._name, "text")

    def _get_for_dict(self, entity):
        return self._get_value(entity)

    def _get_for_index(self, entity):
        return self._get_value(entity)

    def MATCH(self, value):
        return self._name + "_analized_str", Operations.MATCH, value

    def FUZZY(self, value):
        return self._name + "_analized_str", Operations.FUZZY, value


class StringProperty(TextProperty):
    _indexed = True

    def _db_get_value(self, value):
        return value

    def _db_set_value(self, value):
        return value

    def _to_index_defenition(self):
        return "%s %s" % (self._name, "varchar")

    def _get_for_dict(self, entity):
        return self._get_value(entity)