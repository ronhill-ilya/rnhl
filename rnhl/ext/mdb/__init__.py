__version__ = '1.0.10'

__all__ = []


class Future(object):
    @classmethod
    def wait_all(cls, futures):
        pass


def in_transaction():
    return False



def non_transactional(retries=1, xg=False):
    def real_decorator(function):
        def _wrapper(*args, **kwargs):
            function(*args, **kwargs)
        return _wrapper
    return real_decorator


from model import *
from key import *
from properties.primitives import *
from properties.structured import *
from properties.text import *
from properties.blob import *
from properties.serializable import *
from properties.computed import *
from properties.date_time import *
from properties.key import *
from properties.image import *
from context import *
from extention import FlaskMDBExtention
from api import api as admin_api
from flask import request



def get_multi(keys, **kwargs):
    return get_context().get_multi(keys, **kwargs)

def get_multi_async(keys):
    return get_context().get_multi(keys)


def put_multi(entities):
    return [e.put() for e in entities]

def delete_multi(keys):
    ctx = get_context()
    return [ctx.delete(k) for k in keys]


def put_multi_async(keys):
    ctx = get_context()
    return [ctx.put(k) for k in keys]


class FlaskRDBExtension(object):
    def __init__(self, app=None):
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        """

        :param flask.Falask app: flask_app
        :return:
        """
        app.before_request(self._before_request)
        app.after_request(self._after_request)

    def _before_request(self):
        request._ctx = Context()

    def _after_request(self, response):
        request._ctx.after_request(response)
        return response


def admin_api_action(func):
    func.is_api_method = True
    return func
